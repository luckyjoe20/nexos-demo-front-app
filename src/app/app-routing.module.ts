import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookShelfComponent } from './components/book-shelf/book-shelf.component';

const routes: Routes = [    
{path:'bookshelf', component : BookShelfComponent},
{path:'**',pathMatch:'full',redirectTo: 'bookshelf'}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

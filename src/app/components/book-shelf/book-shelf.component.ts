import { Component, OnInit } from '@angular/core';
import { BookService } from "../../services/book-service.service";
import { MatTableDataSource } from '@angular/material/table';

export interface bookDTO {
  author: string,
  gender: string,
  page_count: number,
  publisher: string,
  title: string,
  year: Date
}


@Component({
  selector: 'app-book-shelf',
  templateUrl: './book-shelf.component.html',
  styleUrls: ['./book-shelf.component.css']
})
export class BookShelfComponent implements OnInit {

  displayedColumns: string[] = ['titulo','autor', 'genero', 'cantidad de paginas', 'editor','fecha de publicación'];
  dataSource:any = new MatTableDataSource<bookDTO[]>();
  filterValue:string = "";

  constructor(private bookService:BookService) {

    this.getAllBooks();

   }

  getAllBooks() {
    this.bookService.getAll()
      .subscribe(
        data => {
          this.dataSource = new MatTableDataSource(data as bookDTO[]);
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  ngOnInit(): void {
  }

  public doFilter = (event: any) => {
    this.filterValue = (<HTMLInputElement>event.target).value
    this.dataSource.filter = this.filterValue.trim().toLocaleLowerCase();
  }

}
